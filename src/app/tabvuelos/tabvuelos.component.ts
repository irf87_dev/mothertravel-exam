import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { Cat_TiposVuelos } from './cat_tiposvuelos';
import { CatAeropuertosService } from '../services/cat-aeropuertos.service';
import { Vuelo } from '../shared/vuelo';

import * as M from 'materialize-css';
declare var $:any;
@Component({
  selector: 'app-tabvuelos',
  templateUrl: './tabvuelos.component.html',
  styleUrls: ['./tabvuelos.component.css'],
  providers:[CatAeropuertosService]
})
export class TabvuelosComponent implements OnInit {
  public arrayCatTiposVuelos : any [];
  public arrayCatAeropuetos : any [];
  public model = new Vuelo("Redondo",null,null,null,null,null);
  public submitted = false;
  constructor(
    private catService : CatAeropuertosService,
    private route : ActivatedRoute,
    private router: Router
  ) {
   }

  ngOnInit() {
    M.Autocomplete.init(document.querySelector('#input_salida'),{
      onAutocomplete : (evt) => this.onAutocomplete(this,evt,"salida")
    });

    M.Autocomplete.init(document.querySelector('#input_destino'),{
      onAutocomplete : (evt) => this.onAutocomplete(this,evt,"destino")
    });

    M.Datepicker.init(document.querySelectorAll('#picket_salida'), {
      format : 'dd-mm-yyyy',
      onSelect : (evt) => this.onDateSelected(this,evt,"fecha_salida")
    });

    M.Datepicker.init(document.querySelectorAll('#picket_regreso'), {
      format : 'dd-mm-yyyy',
      onSelect : (evt) => this.onDateSelected(this,evt,"fecha_regreso")
    });

    this.arrayCatTiposVuelos = new Cat_TiposVuelos().get();
    this.loadCat();
  }

  //Custom methods

  loadCat(){
    const elem = document.querySelector('#input_salida');
    const elem2 = document.querySelector('#input_destino');
    let instance = M.Autocomplete.getInstance(elem);
    let instance2 = M.Autocomplete.getInstance(elem2);

    this.catService.get().subscribe(
      (dataSuccess : any) => {
        const { aeropuertos } = dataSuccess;
        if(aeropuertos){
          this.arrayCatAeropuetos = aeropuertos;
          instance.updateData(this.autoCompleFormat(aeropuertos));
          instance2.updateData(this.autoCompleFormat(aeropuertos));
        }
      },
      (dataError : any) =>{
        console.error(dataError);
      }
    );
  }

  autoCompleFormat(array){
    let oAutoComplete = {};
    array.map((item)=>{
      oAutoComplete[item.name] = null;
    });
    return oAutoComplete;
  }

  onSubmit() {
    this.submitted = true;
    this.router.navigate(["/detalle"],{queryParams : this.model});
  }

  onAutocomplete(context,string,key){
    context.model[key] = string;
  }

  onDateSelected(context,date,key){
    context.model[key] = date;
  }

}

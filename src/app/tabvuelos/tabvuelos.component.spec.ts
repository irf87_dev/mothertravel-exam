import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabvuelosComponent } from './tabvuelos.component';

describe('TabvuelosComponent', () => {
  let component: TabvuelosComponent;
  let fixture: ComponentFixture<TabvuelosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabvuelosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabvuelosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class Cat_TiposVuelos{
  private array_cat_tiposvuelos : any = [
    {
      "display" : "Redondo",
      "type" : "Redondo",
    },
    {
      "display" : "Sencillo",
      "type" : "Sencillo",
    },
    {
      "display" : "Multidestino",
      "type" : "Multidestino",
    }
  ]

  constructor(){}

  get(){
    return this.array_cat_tiposvuelos;
  }
}

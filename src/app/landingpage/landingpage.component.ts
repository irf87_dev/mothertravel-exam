import { Component, OnInit } from '@angular/core';

declare var $:any;
@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.css'],
  providers : []
})
export class LandingpageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.tabs').tabs();
  }

}

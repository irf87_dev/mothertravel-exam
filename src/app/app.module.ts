import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { TabvuelosComponent } from './tabvuelos/tabvuelos.component';
import { DetalleComponent } from './detalle/detalle.component';
import { FechasPipe } from './pipes/fechas.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LandingpageComponent,
    TabvuelosComponent,
    DetalleComponent,
    FechasPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

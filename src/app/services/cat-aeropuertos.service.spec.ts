import { TestBed } from '@angular/core/testing';

import { CatAeropuertosService } from './cat-aeropuertos.service';

describe('CatAeropuertosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CatAeropuertosService = TestBed.get(CatAeropuertosService);
    expect(service).toBeTruthy();
  });
});

import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';

@Injectable()
export class CatAeropuertosService {
  private host: string = environment.api_url;

  constructor(private http: HttpClient) { }

  get(){
    return this.http.get<any>(this.host);
  }
}

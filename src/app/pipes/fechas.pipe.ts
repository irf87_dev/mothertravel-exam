import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'fechas'
})
export class FechasPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let dateFormat = "DD-MM-YYYY";
    let dateStr = value;
    if(value){
        dateStr = moment(new Date(value)).format(dateFormat);
      }
    return dateStr;
  }

}

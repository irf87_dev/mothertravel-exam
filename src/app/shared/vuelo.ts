export class Vuelo {
  constructor(
    public tipo: string,
   public salida: string,
   public destino: string,
   public fecha_salida: any,
   public fecha_regreso: any,
   public pasajeros: number
 ) {  }
}
